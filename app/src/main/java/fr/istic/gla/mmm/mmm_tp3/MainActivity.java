package fr.istic.gla.mmm.mmm_tp3;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;

import fr.istic.gla.mmm.mmm_tp3.Domaine.WineProperties;
import fr.istic.gla.mmm.mmm_tp3.Fragment.ListFragment;
import fr.istic.gla.mmm.mmm_tp3.Fragment.WebViewFragment;

public class MainActivity extends AppCompatActivity implements ListFragment.OnItemSelectedListener, WebViewFragment.IOnButtonLocateClick{

    private boolean small_mode = true;
    private Fragment webViewFragment;

    public static final String WINE_PROPERTIES = "WINE_PROPERTIES";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        small_mode = findViewById(R.id.layout_smartphone) != null;

        if ( small_mode ){
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            if ( fragmentTransaction != null ){
                Fragment fragment = new ListFragment();
                ((ListFragment) fragment).setListener(this);
                fragmentTransaction.add(R.id.framelayout, fragment);
                fragmentTransaction.addToBackStack(null);
            }

            if (fragmentTransaction != null) fragmentTransaction.commit();
        } else {

            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            if (fragmentTransaction != null) {
                Fragment listFragment = new ListFragment();
                ((ListFragment) listFragment).setListener(this);
                fragmentTransaction.add(R.id.listFrameLayout, listFragment);

                webViewFragment = new WebViewFragment();
                ((WebViewFragment) webViewFragment).setListener(this);
                fragmentTransaction.add(R.id.webViewFrameLayout, webViewFragment);

            }

            if (fragmentTransaction != null) fragmentTransaction.commit();
        }
    }

    @Override
    public void onBackPressed() {

        if ( small_mode ) {
            FragmentManager fragmentManager = getFragmentManager();

            if (fragmentManager.getBackStackEntryCount() > 1)
                fragmentManager.popBackStack();
        } else
            if ( webViewFragment != null )
                ((WebViewFragment) webViewFragment).backHistory();

    }

    @Override
    public void onItemSelected(WineProperties wineProperties) {
        if ( small_mode ) {

            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            if ( fragmentTransaction != null ) {
                Bundle args = new Bundle();
                args.putParcelable(WINE_PROPERTIES, wineProperties);
                Fragment fragment = new WebViewFragment();
                ((WebViewFragment) fragment).setListener(this);
                fragment.setArguments(args);
                fragmentTransaction.replace(R.id.framelayout, fragment);
                fragmentTransaction.addToBackStack(null);
            }

            if (fragmentTransaction != null)
                fragmentTransaction.commit();

        } else {

            FragmentManager fragmentManager = getFragmentManager();
            Fragment fragment = fragmentManager.findFragmentById(R.id.webViewFrameLayout);
            if (fragment.getClass() != WebViewFragment.class){
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                if ( fragmentTransaction != null ) {
                    ((WebViewFragment) webViewFragment).setWineProperties(wineProperties);
                    fragmentTransaction.replace(R.id.webViewFrameLayout, webViewFragment);
                    fragmentTransaction.addToBackStack(null);
                }

                if (fragmentTransaction != null)
                    fragmentTransaction.commit();
            } else {
                ((WebViewFragment) webViewFragment).loadUrl(wineProperties.getUrl());
                ((WebViewFragment) webViewFragment).bindButton(wineProperties.getName(), wineProperties.getPosition());
            }
        }
    }

    @Override
    public void OnButtonLocateClick(final String name,final LatLng position) {
        if ( small_mode ) {
            Intent intent = new Intent(this, MapActivity.class);
            intent.putExtra("LAT_LNG", position);
            intent.putExtra("NAME", name);
            startActivity(intent);
        } else {

            MapFragment fragment = MapFragment.newInstance();

            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

            if ( fragmentTransaction != null ){
                fragmentTransaction.replace(R.id.webViewFrameLayout, fragment);
                fragmentTransaction.addToBackStack(null);
            }

            if (fragmentTransaction != null)
                fragmentTransaction.commit();

            fragment.getMapAsync(new CustomOnMapReadyCallback(name,position));
        }
    }
}
