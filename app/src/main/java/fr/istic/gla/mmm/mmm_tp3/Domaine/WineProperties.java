package fr.istic.gla.mmm.mmm_tp3.Domaine;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by lautre on 20/03/16.
 */
public class WineProperties implements Parcelable {

    private String name;
    private String url;
    private LatLng position;

    public WineProperties(String name, String url,LatLng position) {
        this.name = name;
        this.url = url;
        this.position = position;
    }

    protected WineProperties(Parcel in) {
        name = in.readString();
        url = in.readString();
        position = in.readParcelable(LatLng.class.getClassLoader());
    }

    public static final Creator<WineProperties> CREATOR = new Creator<WineProperties>() {
        @Override
        public WineProperties createFromParcel(Parcel in) {
            return new WineProperties(in);
        }

        @Override
        public WineProperties[] newArray(int size) {
            return new WineProperties[size];
        }
    };

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LatLng getPosition() {
        return position;
    }

    public void setPosition(LatLng position) {
        this.position = position;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(url);
        dest.writeParcelable(position, flags);
    }
}
