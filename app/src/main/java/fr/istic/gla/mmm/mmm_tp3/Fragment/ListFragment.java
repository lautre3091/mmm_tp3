package fr.istic.gla.mmm.mmm_tp3.Fragment;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import fr.istic.gla.mmm.mmm_tp3.Domaine.WineProperties;
import fr.istic.gla.mmm.mmm_tp3.R;

public class ListFragment extends android.app.Fragment {
    private static final String TAG = WebViewFragment.class.getSimpleName();

    private OnItemSelectedListener listener;

    private static List<WineProperties> data = new ArrayList<>();

    static {
        data.add(new WineProperties("Alsace", "http://technoresto.org/vdf/alsace/index.shtml" ,new LatLng(48.2454402,6.4161316)));
        data.add(new WineProperties("Beaujolais", "http://technoresto.org/vdf/beaujolais/index.html",new LatLng(46.083347,4.6579122)));
        data.add(new WineProperties("Jura", "http://technoresto.org/vdf/jura/index.html",new LatLng(46.7828921,5.1688018)));
        data.add(new WineProperties("Champagne", "http://technoresto.org/vdf/champagne/index.html",new LatLng(49.253642,3.9850484)));
        data.add(new WineProperties("Savoie", "http://technoresto.org/vdf/savoie/index.html",new LatLng(45.4947055,5.8432796)));
        data.add(new WineProperties("Languedoc-Roussillon", "http://technoresto.org/vdf/languedoc/index.html",new LatLng(43.636424,1.0234522)));
        data.add(new WineProperties("Bordelais", "http://technoresto.org/vdf/bordelais/index.html",new LatLng(44.8638282,-0.6561813)));
        data.add(new WineProperties("Vallée du Rhone", "http://technoresto.org/vdf/cotes_du_rhone/index.html",new LatLng(44.6835657,3.7349573)));
        data.add(new WineProperties("Provence", "http://technoresto.org/vdf/provence/index.html",new LatLng(46.097262,4.5722252)));
        data.add(new WineProperties("Val de Loire", "http://technoresto.org/vdf/val_de_loire/index.html",new LatLng(47.9135477,1.7594827)));
        data.add(new WineProperties("Sud-Ouest", "http://technoresto.org/vdf/sud-ouest/index.html",new LatLng(44.8802308,-1.5943457)));
        data.add(new WineProperties("Corse", "http://technoresto.org/vdf/corse/index.html",new LatLng(42.1771653,7.9287458)));
        data.add(new WineProperties("Bourgogne", "http://technoresto.org/vdf/bourgogne/index.html",new LatLng(47.2744622,3.0612084)));
    }

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle args) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);

        ListView list = (ListView) view.findViewById(R.id.listview);
        list.setAdapter(
                new ArrayAdapter(
                        view.getContext(),
                        android.R.layout.simple_list_item_1,
                        android.R.id.text1,
                        getNames(data)
                )
        );

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String text = ((TextView) view.findViewById(android.R.id.text1)).getText().toString();
                WineProperties wineProperties = data.get(position);

                if (wineProperties != null && wineProperties.getName().equals(text))
                    listener.onItemSelected(wineProperties);
                else
                    Log.e(TAG,"Error on Item");
            }
        });

        return view;
    }

    public void setListener( OnItemSelectedListener listener ){
        this.listener = listener;
    }

    public interface OnItemSelectedListener {
        void onItemSelected(WineProperties wineProperties);
    }

    private List<String> getNames(List<WineProperties> winePropertiesList){
        List<String> names = new ArrayList<>();
        for (WineProperties wineProperties : winePropertiesList) names.add(wineProperties.getName());
        return names;
    }
}
