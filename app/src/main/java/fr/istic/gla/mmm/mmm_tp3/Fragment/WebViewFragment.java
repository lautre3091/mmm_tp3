package fr.istic.gla.mmm.mmm_tp3.Fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import fr.istic.gla.mmm.mmm_tp3.Domaine.WineProperties;
import fr.istic.gla.mmm.mmm_tp3.MainActivity;
import fr.istic.gla.mmm.mmm_tp3.R;

public class WebViewFragment extends Fragment {
    private static final String TAG = WebViewFragment.class.getSimpleName();

    private TextView information;

    private WebView webView;

    private String url;

    private Button btnGoToMap;

    private IOnButtonLocateClick listener;

    private WineProperties wineProperties = null;

    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle args) {
        View view = inflater.inflate(R.layout.fragment_web_view, container, false);

        Bundle arguments = getArguments();

        webView = (WebView) view.findViewById(R.id.webView);
        information = (TextView) view.findViewById(R.id.information);
        btnGoToMap = (Button) view.findViewById(R.id.btn_go_to_map);


        if ( arguments != null && arguments.containsKey(MainActivity.WINE_PROPERTIES))
            wineProperties = arguments.getParcelable(MainActivity.WINE_PROPERTIES);

        if (wineProperties != null) {
            loadUrl(wineProperties.getUrl());
            bindButton(wineProperties.getName(),wineProperties.getPosition());
        }
        else
            Log.e(TAG, "Error on getting argument");


        return view;
    }

    public void bindButton(final String name,final LatLng position){
        btnGoToMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ( listener != null )
                    listener.OnButtonLocateClick(name, position);
            }
        });
    }

    public void loadUrl( String url ){
        if ( url != null && !url.isEmpty() ){
            if ( !url.equals(this.url) ) {
                webView.loadUrl(url);
                this.url = url;
            }
            information.setVisibility(View.GONE);
            webView.setVisibility( View.VISIBLE );
        }else{
            information.setVisibility( View.VISIBLE );
            webView.setVisibility( View.GONE );
        }
    }

    public void setListener( IOnButtonLocateClick listener ){
        this.listener = listener;
    }

    public interface IOnButtonLocateClick {
        void OnButtonLocateClick(String name, LatLng position);
    }

    public void backHistory(){
        this.webView.goBack();
    }

    public void setWineProperties(WineProperties wineProperties) {
        this.wineProperties = wineProperties;
    }
}
