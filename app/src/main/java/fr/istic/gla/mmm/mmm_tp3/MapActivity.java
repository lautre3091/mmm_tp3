package fr.istic.gla.mmm.mmm_tp3;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapActivity extends AppCompatActivity {

    private MapFragment currentFragment;

    private LatLng position;

    private String name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        currentFragment = MapFragment.newInstance();

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        Intent intent = getIntent();
        position = intent.getParcelableExtra("LAT_LNG");
        name = intent.getStringExtra("NAME");

        if (fragmentTransaction != null) {
            fragmentTransaction.add(R.id.mapFragment, currentFragment);
            fragmentTransaction.addToBackStack(null);
        }

        if (fragmentTransaction != null) fragmentTransaction.commit();

        currentFragment.getMapAsync(new CustomOnMapReadyCallback(name,position));
    }
}
