
package fr.istic.gla.mmm.mmm_tp3;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Created by lautre on 20/03/16.
 */
public class CustomOnMapReadyCallback implements OnMapReadyCallback {

    private LatLng latLng;
    private String name;

    public CustomOnMapReadyCallback(String name,LatLng latLng) {
        this.latLng = latLng;
        this.name = name;
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        googleMap.addMarker(
                new MarkerOptions()
                        .position(latLng)
                        .title(name)
        );
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
    }
}
